/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usermanamentproject;

import java.io.Serializable;

/**
 *
 * @author User
 */
public class User implements Serializable{
    private int id;
    private String login;
    private String name;
    private String passwoed;
    private char gender; //M,F
    private char role; // A,U
   
 
    public User(String login, String name, String passwoed, char gender, char role) {
       this(-1, login, name, passwoed, gender, role);
    }
    public User(int id, String login, String name, String passwoed, char gender, char role) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.passwoed = passwoed;
        this.gender = gender;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPasswoed() {
        return passwoed;
    }

    public void setPasswoed(String passwoed) {
        this.passwoed = passwoed;
    }

    public char getGender() {
        return gender;
    }
    public String getGenderString() {
        if(gender == 'M'){
            return "Male";
        }else{
            return  "Female";
        }
    }
    public void setGender(char gender) {
        this.gender = gender;
    }

    public char getRole() {
        return role;
    }
     public String getRoleString() {
        if(role == 'A'){
            return "Admin";
        }else{
            return  "User";
        }
    }
    public void setRole(char role) {
        this.role = role;
    }

   

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", login=" + login + ", name=" + name + ", passwoed=" + passwoed + ", gender=" + gender + ", role=" + role + '}';
    }
    
}
